import { Component, OnInit, Renderer2, RendererFactory2, AfterContentInit } from '@angular/core';
import { faMicrosoft, faJava, faJs, faAdobe, faAndroid, faAngular, faNodeJs,
  faHtml5, faCss3Alt} from '@fortawesome/free-brands-svg-icons';
import { faDatabase, faCode, faPalette, faPenSquare, faChartLine, faCamera,
faBullseye, faEye, faCogs} from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-brief',
  templateUrl: './brief.component.html',
  styleUrls: ['./brief.component.css']
})
export class BriefComponent implements OnInit, AfterContentInit {
  private  renderer: Renderer2;
  tabButtons: HTMLCollectionOf<Element>;
  tabContent: HTMLCollectionOf<Element>;

  microsoft = faMicrosoft;
  java = faJava;
  javascript = faJs;
  adobe = faAdobe;
  android = faAndroid;
  angular = faAngular;
  nodejs = faNodeJs;
  html = faHtml5;
  css = faCss3Alt;
  database = faDatabase;
  code = faCode;
  graphics = faPalette;
  drawing = faPenSquare;
  marketing = faChartLine;
  photography = faCamera;
  vision = faEye;
  mission = faBullseye;
  values = faCogs;

  constructor(private rendererFactory: RendererFactory2, private _spinner: NgxSpinnerService) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  ngOnInit() {
    this._spinner.show();
  }

  ngAfterContentInit(): void {
    this.openTab(1);
    this._spinner.hide();
  }

  openTab(tab: number) {
    this.tabContent = document.getElementsByClassName('tab-content');
    this.tabButtons =  document.getElementsByClassName('tab-button');

    for (let i = 0; i < this.tabButtons.length; i++) {
      this.renderer.setStyle(this.tabContent[i], 'display', 'none');
      this.renderer.removeClass(this.tabButtons[i], 'active');
    }

    switch (tab) {
      case(1): {
        document.getElementById('services').style.display = 'block';
        this.renderer.addClass(this.tabButtons[0], 'active');
        break;
      }
      case(2): {
        document.getElementById('drive').style.display = 'block';
        this.renderer.addClass(this.tabButtons[1], 'active');
        break;
      }
      case(3): {
        document.getElementById('tools').style.display = 'block';
        this.renderer.addClass(this.tabButtons[2], 'active');
        break;
      }
     
      default: {
        break;
      }
    }
  }
}
