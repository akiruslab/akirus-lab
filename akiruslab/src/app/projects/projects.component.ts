import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Categories } from '../categories';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})

export class ProjectsComponent implements OnInit, AfterContentInit{
  public drawingView;
  public photosView;
  public videosView;
  public closePhotos ;
  public closeVideos ;
  public closeDrawings;

  listOfCategories = Categories.Category;

  constructor(private _spinner: NgxSpinnerService) { }

  ngOnInit() {
    this._spinner.show();

    // Get the modal
    this.drawingView = document.getElementById('drawings-view');
    this.photosView = document.getElementById('photos-view');
    this.videosView = document.getElementById('videos-view');

    // Get the <span> element that closes the modal
    this.closeDrawings = document.getElementsByClassName('close')[0];
    this.closePhotos = document.getElementsByClassName('close')[1];
    this.closeVideos = document.getElementsByClassName('close')[2];
  }

  ngAfterContentInit(): void {
    this._spinner.hide();
  }

  onClick(category: string) {
    const currentContext = this;

    console.log(category);
    switch (category) {
      case this.listOfCategories.DRAWINGS:
      {
        this.drawingView.style.display = 'block';

        // When the user clicks on <span> (x), close the modal
        this.closeDrawings.onclick = () => {
          currentContext.drawingView.style.display = 'none';
        };

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target === currentContext.drawingView) {
            currentContext.drawingView.style.display = 'none';
          }
        };

        break;
      }
      case this.listOfCategories.PHOTOS:
      {
        this.photosView.style.display = 'block';
        // When the user clicks on <span> (x), close the modal
        this.closePhotos.onclick = () => {
          currentContext.photosView.style.display = 'none';
        };

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target === currentContext.photosView) {
            currentContext.photosView.style.display = 'none';
          }
        };

        break;
      }
      case this.listOfCategories.VIDEOS:
      {
        this.videosView.style.display = 'block';
        // When the user clicks on <span> (x), close the modal
        this.closeVideos.onclick = () => {
          currentContext.videosView.style.display = 'none';
        };

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target === currentContext.videosView) {
            currentContext.videosView.style.display = 'none';
          }
        };

        break;
      }
      default:
      {
        break;
      }
    }
  }
}
