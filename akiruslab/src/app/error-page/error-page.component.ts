import { Component, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements AfterViewInit {

  constructor(private _spinner: NgxSpinnerService) { }

  ngAfterViewInit() {
    this._spinner.hide();
  }
}
