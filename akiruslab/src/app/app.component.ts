import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit{
  title = 'akiruslab';

  constructor(private _spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this._spinner.show();
  }

  ngAfterViewInit(): void {
    this._spinner.hide();
  }
}
