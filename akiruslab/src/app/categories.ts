export module Categories {
  export enum Category {
    CODE= 'CODE',
    DRAWINGS = 'DRAWINGS',
    PHOTOS = 'PHOTOS',
    VIDEOS = 'VIDEOS',
    LITERATURE = 'WRITING',
  }
}


