import { Injectable } from '@angular/core';
import { MediaFile} from './media-model';
import { Photos } from './projects-photos/photo-types';
import { DrawingType } from './projects-drawings/drawing-types';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  drawingCategories = DrawingType.TYPES;
  pictureCategories = Photos.TYPES;

  animalCollection: MediaFile[];
  animalThumbnails: MediaFile[];
  eventCollection: MediaFile[];
  eventThumbnails: MediaFile[];
  gamingCollection: MediaFile[];
  gamingThumbnails: MediaFile[];
  placesCollection: MediaFile[];
  placesThumbnails: MediaFile[];
  peopleCollection: MediaFile[];
  peopleThumbnails: MediaFile[];


  compositionCollection: MediaFile[];
  natureCollection: MediaFile[];
  portraitCollection: MediaFile[];

  constructor() {
    this.animalCollection = [];
    this.eventCollection = [];
    this.gamingCollection = [];
    this. placesCollection = [];
    this.peopleCollection = [];

    this.animalThumbnails = [];
    this.eventThumbnails = [];
    this.gamingThumbnails = [];
    this.placesThumbnails = [];
    this.peopleThumbnails = [];

    this.compositionCollection = [];
    this.natureCollection = [];
    this.portraitCollection = [];
  }

  getPhotoImages(category: string) {
    let position = 0;

    switch (category) {
      case this.pictureCategories.ANIMALS:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.animalCollection.push( new MediaFile( position, 'Image ' + position,
          '../../assets/media/images/photos/animals/landscape/' + position + '.jpg'));
        }

        return this.animalCollection;
      }
      case this.pictureCategories.EVENTS:
      {

        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.eventCollection.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/events/landscape/' + (position) + '.jpg'));
        }

        return this.eventCollection;
      }
      case this.pictureCategories.GAMING:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.gamingCollection.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/gaming/' + (position) + '.jpg'));
        }

        return this.gamingCollection;
      }
      case this.pictureCategories.PLACES:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.placesCollection.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/places/landscape/' + (position) + '.jpg'));
        }

        return this.placesCollection;
      }
      case this.pictureCategories.PEOPLE:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.peopleCollection.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/people/landscape/' + (position) + '.jpg'));
        }

        return this.peopleCollection;
      }
      default: {
        break;
      }
    }
  }

  getPhotoThumbnails(category: string) {
    let position = 0;

    switch (category) {
      case this.pictureCategories.ANIMALS:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.animalThumbnails.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/animals/landscape/' + position + '_thumbnail.jpg'));
        }

        return this.animalCollection;
      }
      case this.pictureCategories.EVENTS:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.eventThumbnails.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/events/landscape/' + (position) + '_thumbnail.jpg'));
        }

        return this.eventCollection;
      }
      case this.pictureCategories.GAMING:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.gamingThumbnails.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/gaming/' + (position) + '_thumbnail.jpg'));
        }

        return this.gamingCollection;
      }
      case this.pictureCategories.PLACES:
      {
        for (let i = 0; i < 6; i++) {
          this.placesThumbnails.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/places/landscape/' + (position) + '_thumbnail.jpg'));
        }

        return this.placesCollection;
      }
      case this.pictureCategories.PEOPLE:
      {
        for (let i = 0; i < 6; i++) {
          position = i + 1;
          this.peopleThumbnails.push( new MediaFile( position , 'Image ' + position,
          '../../assets/media/images/photos/people/landscape/' + (position) + '_thumbnail.jpg'));
        }

        return this.peopleCollection;
      }
      default: {
        break;
      }
    }
  }

  getAllNatureDrawingImages () {

    if (this.natureCollection.length === 0) {
      this.natureCollection.push(new MediaFile(1, 'Two Worlds Reflect', '../../assets/media/images/drawings/nature/' + (1) + '.PNG'));
      this.natureCollection.push(new MediaFile(2, 'Ice Beach', '../../assets/media/images/drawings/nature/' + (2) + '.PNG'));
      this.natureCollection.push(new MediaFile(3, 'Man Bowl', '../../assets/media/images/drawings/nature/' + (3) + '.jpg'));
      this.natureCollection.push(new MediaFile(4, 'Big Attractions', '../../assets/media/images/drawings/nature/' + (4) + '.PNG'));
    }

    return this.natureCollection;
  }

  getAllPortraitDrawingImages () {

    if (this.portraitCollection.length === 0 ) {
      this.portraitCollection.push( new MediaFile(1, 'Joy', '../../assets/media/images/drawings/portraits/' + (1) + '.jpg'));
      this.portraitCollection.push( new MediaFile(2, 'Thoughts', '../../assets/media/images/drawings/portraits/' + (2) + '.jpg'));
      this.portraitCollection.push( new MediaFile(3, 'Strength', '../../assets/media/images/drawings/portraits/' + (3) + '.jpg'));
      this.portraitCollection.push( new MediaFile(4, 'Elegance', '../../assets/media/images/drawings/portraits/' + (4) + '.jpg'));
      this.portraitCollection.push( new MediaFile(5, 'Captain', '../../assets/media/images/drawings/portraits/' + (5) + '.jpg'));
      this.portraitCollection.push( new MediaFile(6, 'Homies', '../../assets/media/images/drawings/portraits/' + (6) + '.PNG'));
      this.portraitCollection.push( new MediaFile(7, 'Freedom Fighters', '../../assets/media/images/drawings/portraits/' + (7) + '.PNG'));
    }

    return this.portraitCollection;
  }

  getAllCompositionalDrawingImages () {

    if (this.compositionCollection.length === 0) {
      this.compositionCollection.push( new MediaFile(2, '16', '../../assets/media/images/drawings/compositions/' + (1) + '.PNG'));
      this.compositionCollection.push( new MediaFile(3, 'Germany', '../../assets/media/images/drawings/compositions/' + (2) + '.PNG'));
      this.compositionCollection.push( new MediaFile(4, 'Greece Lightning',
      '../../assets/media/images/drawings/compositions/' + (3) + '.PNG'));
      this.compositionCollection.push( new MediaFile(5, 'Homeless Homies',
       '../../assets/media/images/drawings/compositions/' + (4) + '.PNG'));
      this.compositionCollection.push( new MediaFile(6, 'Italian Job', '../../assets/media/images/drawings/compositions/' + (5) + '.PNG'));
      this.compositionCollection.push( new MediaFile(6, 'Today vs Yesterday',
      '../../assets/media/images/drawings/compositions/' + (6) + '.PNG'));
    }

    return this.compositionCollection;
  }
}
