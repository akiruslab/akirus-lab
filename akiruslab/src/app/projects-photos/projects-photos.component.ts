import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MediaFile} from '../media-model';
import { Photos } from './photo-types';
import { ImageSliderService } from '../image-slider.service';
import { ImageService } from '../image.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-projects-photos',
  templateUrl: './projects-photos.component.html',
  styleUrls: ['./projects-photos.component.css']
})

export class ProjectsPhotosComponent implements OnInit, AfterViewInit {
  pictureCategories = Photos.TYPES;

  collection: MediaFile[];
  thumbnails: MediaFile[];

  animalCollection: MediaFile[];
  animalThumbnails: MediaFile[];
  eventCollection: MediaFile[];
  eventThumbnails: MediaFile[];
  gamingCollection: MediaFile[];
  gamingThumbnails: MediaFile[];
  placesCollection: MediaFile[];
  placesThumbnails: MediaFile[];
  peopleCollection: MediaFile[];
  peopleThumbnails: MediaFile[];

  constructor(private _imageService: ImageService, private _change: ChangeDetectorRef,
    public slider: ImageSliderService, private _spinner: NgxSpinnerService) {}

  ngOnInit() {
    this._spinner.show();
  }

  ngAfterViewInit(): void {
    this.loadImages();
    this.loadCategory(this.pictureCategories.ANIMALS);
    this._spinner.hide();
  }

  loadCategory(type: string) {
    switch (type) {
      case this.pictureCategories.ANIMALS:
      {
        this.collection = this.animalCollection;
        this.thumbnails = this.animalThumbnails;

        this._change.detectChanges();
        break;
      }
      case this.pictureCategories.EVENTS:
      {
        this.collection = this.eventCollection;
        this.thumbnails = this.eventThumbnails;

        this._change.detectChanges();
        break;
      }
      case this.pictureCategories.GAMING:
      {
        this.collection = this.gamingCollection;
        this.thumbnails = this.gamingThumbnails;

        this._change.detectChanges();
        break;
      }
      case this.pictureCategories.PLACES:
      {
        this.collection = this.placesCollection;
        this.thumbnails = this.placesThumbnails;

        this._change.detectChanges();
        break;
      }
      case this.pictureCategories.PEOPLE:
      {
        this.collection = this.peopleCollection;
        this.thumbnails = this.peopleThumbnails;

        this._change.detectChanges();
        break;
      }
      default: {
        break;
      }
    }

    this.slider.currentSlide(1, 'photoSlide');
  }

  loadImages() {
    this.animalCollection = this._imageService.getPhotoImages(this.pictureCategories.ANIMALS);
    this.eventCollection = this._imageService.getPhotoImages(this.pictureCategories.EVENTS);
    this.gamingCollection = this._imageService.getPhotoImages(this.pictureCategories.GAMING);
    this.placesCollection = this._imageService.getPhotoImages(this.pictureCategories.PLACES);
    this.peopleCollection = this._imageService.getPhotoImages(this.pictureCategories.PEOPLE);

    this.animalThumbnails = this._imageService.getPhotoThumbnails(this.pictureCategories.ANIMALS);
    this.eventThumbnails = this._imageService.getPhotoThumbnails(this.pictureCategories.EVENTS);
    this.gamingThumbnails = this._imageService.getPhotoThumbnails(this.pictureCategories.GAMING);
    this.placesThumbnails = this._imageService.getPhotoThumbnails(this.pictureCategories.PLACES);
    this.peopleThumbnails = this._imageService.getPhotoThumbnails(this.pictureCategories.PEOPLE);
  }
}
