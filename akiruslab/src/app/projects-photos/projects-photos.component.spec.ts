import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsPhotosComponent } from './projects-photos.component';

describe('ProjectsPhotosComponent', () => {
  let component: ProjectsPhotosComponent;
  let fixture: ComponentFixture<ProjectsPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
