export module Photos {
  export enum TYPES {
    ANIMALS = 'ANIMALS',
    GAMING = 'GAMING',
    PEOPLE = 'PEOPLE',
    PLACES = 'PLACES',
    EVENTS = 'EVENTS',
    STILL = 'STILL-LIFE',
  }
}
