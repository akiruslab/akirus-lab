import { Component, OnInit, AfterContentInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterContentInit {

  constructor(private _spinner: NgxSpinnerService) { }

  ngOnInit() {
    this._spinner.show();
  }

  ngAfterContentInit(): void {
    this._spinner.hide();
  }
}
