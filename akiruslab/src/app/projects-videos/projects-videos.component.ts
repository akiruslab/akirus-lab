import { Component, AfterContentInit, OnChanges } from '@angular/core';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { MediaFile} from '../media-model';
import { Videos } from './video-types';
import { ImageSliderService } from '../image-slider.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-projects-videos',
  templateUrl: './projects-videos.component.html',
  styleUrls: ['./projects-videos.component.css']
})
export class ProjectsVideosComponent implements OnChanges, AfterContentInit {
  videoCategories = Videos.TYPES;

  video: MediaFile;
  url: SafeResourceUrl;

  constructor(private sanitizer: DomSanitizer, public slider: ImageSliderService, private _spinner: NgxSpinnerService) {
    this.video = new MediaFile(0, '', '');
    this.url = this.sanitizeurl('https://www.youtube.com/embed/videoseries?list=PLsv8EG98NN2G8zy1LupzNpii3OmeiSy4d&rel=0');
  }

  ngOnChanges() {
    this._spinner.show();
  }

  ngAfterContentInit(): void  {
    this._spinner.hide();
  }

  loadVideos(type: string) {
    switch (type) {
      case this.videoCategories.EVENTS: {
        this.video.id = 1;
        this.video.path = 'https://www.youtube.com/embed/videoseries?list=PLsv8EG98NN2G8zy1LupzNpii3OmeiSy4d&rel=0';
        this.url = this.sanitizeurl(this.video.path);
        break;
      }
      case this.videoCategories.MUSIC: {
        this.video.id = 2;
        this.video.path = 'https://www.youtube.com/embed/videoseries?list=PLsv8EG98NN2HoEr9OJ0J7FMi1-A27g3Xa&rel=0';
        this.url = this.sanitizeurl(this.video.path);
        break;
      }
      case this.videoCategories.REVIEWS: {
        this.video.id = 3;
        this.video.path = 'https://www.youtube.com/embed/videoseries?list=PLsv8EG98NN2FbD94WZ3qeuoRbW3asv4tc&rel=0';
        this.url = this.sanitizeurl(this.video.path);
        break;
      }
      case this.videoCategories.SERIES: {
        this.video.id = 4;
        this.video.path = 'https://www.youtube.com/embed/videoseries?list=PLsv8EG98NN2G9kFsHlXHgAs0RQVJlqSsS&rel=0';
        this.url = this.sanitizeurl(this.video.path);
        break;
      }
      case this.videoCategories.THOUGHTS: {
        this.video.id = 5;
        this.video.path = 'https://www.youtube.com/embed/videoseries?list=PLsv8EG98NN2Em2FAcQsapAajeBX4ZGDTy&rel=0';
        this.url = this.sanitizeurl(this.video.path);
        break;
      }
      default: {
        break;
      }
    }
  }

  sanitizeurl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  showCategory(type: string) {
    this.loadVideos(type);
  }

}
