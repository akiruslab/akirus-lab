import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsVideosComponent } from './projects-videos.component';

describe('ProjectsVideosComponent', () => {
  let component: ProjectsVideosComponent;
  let fixture: ComponentFixture<ProjectsVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
