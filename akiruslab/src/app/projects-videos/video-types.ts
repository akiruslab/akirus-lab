export module Videos {
  export enum TYPES {
    EVENTS = 'EVENTS',
    MUSIC = 'MUSIC',
    REVIEWS = 'REVIEWS',
    SERIES = 'SERIES',
    THOUGHTS = 'THOUGHTS'
  }
}
