import { Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Renderer2} from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule, routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule} from '@angular/forms';
import { FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import { ContactComponent } from './contact/contact.component';
import { ProjectsComponent } from './projects/projects.component';
import { BriefComponent } from './brief/brief.component';
import { BioComponent } from './bio/bio.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BannerComponent } from './banner/banner.component';

import { ImageSliderService } from './image-slider.service';
import { MailService } from './mail.service';
import { NgxSpinnerModule } from 'ngx-spinner';

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'bio', component: BioComponent},
  {path: 'brief', component: BriefComponent},
  {path: 'info', component: BriefComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'contact', component: ContactComponent},
  {path: '', component: HomeComponent},
  {path: '**', component: HomeComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    FooterComponent,
    HomeComponent,
    NavigationComponent,
    BannerComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  providers: [ImageSliderService, MailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
