import { Component, OnChanges, AfterViewChecked } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.css']
})
export class BioComponent implements OnChanges, AfterViewChecked {

  constructor(private _spinner: NgxSpinnerService) { }
  ngOnChanges() {
    this._spinner.show();
  }

  ngAfterViewChecked() {
    this._spinner.hide();
  }
}
