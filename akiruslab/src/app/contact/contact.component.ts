import { Component, OnChanges, AfterViewChecked } from '@angular/core';
import { NgForm} from '@angular/forms';
import { MailService} from '../mail.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { faBitbucket, faYoutube, faLinkedin, faInstagram} from '@fortawesome/free-brands-svg-icons';
import { faPhoneSquareAlt, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnChanges, AfterViewChecked{
  private mailService: Subscription;
  errorMessage = '';
  bitbucket = faBitbucket;
  youtube = faYoutube;
  linkedin = faLinkedin;
  instagram = faInstagram;
  phone = faPhoneSquareAlt;
  sendMail = faPaperPlane;

  mail = {
    name: '',
    email: '',
    number: '',
    subject: '',
    message: '',
    sendTo: ''
  };

  constructor(private _mailService: MailService, private _spinner: NgxSpinnerService) { }

  ngOnChanges(): void {
    this._spinner.show();
  }

  ngAfterViewChecked(): void {
    this._spinner.hide();
  }

  resetForm(form: NgForm) {
    form.value.message = '';
  }

  onSubmit(contactForm: NgForm) {
    this.mail = contactForm.value;
    this.mail.sendTo = 'info@akiruslab.com';

   if (contactForm.invalid === null || contactForm.invalid) {
      this.errorMessage = 'Please fill in the form before sending a message';

    } else {

        this._spinner.show();

        this.mailService = this._mailService.sendMail(this.mail).subscribe(data => {
          const response = data.toString();
          console.log('Received:' + response + '...');

          if (response === '') {
            this.errorMessage = 'An error occured. Please try again or contact me directly';

          } else if (response === 'success') {
            const image = <HTMLImageElement> document.getElementById('contact-img');
            image.src = '../../assets/media/images/send-letter.svg';
            this.resetForm(contactForm);
            this.mailService.unsubscribe();
          }
          this._spinner.hide();
        });
    }
  }
}
