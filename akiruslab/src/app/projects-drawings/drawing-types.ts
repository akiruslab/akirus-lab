export module DrawingType {
  export enum TYPES {
    COMPOSITIONS = 'COMPOSITIONS',
    NATURE = 'NATURE',
    PORTRAITS = 'PORTRAITS',
  }
}
