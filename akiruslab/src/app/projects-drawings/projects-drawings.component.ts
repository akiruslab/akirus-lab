import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MediaFile} from '../media-model';
import { DrawingType } from './drawing-types';
import { ImageService} from '../image.service';
import { ImageSliderService } from '../image-slider.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-projects-drawings',
  templateUrl: './projects-drawings.component.html',
  styleUrls: ['./projects-drawings.component.css']
})

export class ProjectsDrawingsComponent implements OnInit, AfterViewInit {
  drawingCategories = DrawingType.TYPES;
  collection: MediaFile[];
  compositionCollection: MediaFile[];
  natureCollection: MediaFile[];
  portraitCollection: MediaFile[];

  constructor(public slider: ImageSliderService, private _imageService: ImageService,
    private _change: ChangeDetectorRef, private _spinner: NgxSpinnerService) {}

  ngOnInit() {
    this._spinner.show();
  }

  ngAfterViewInit(): void {
    this.loadImages();
    this.loadCategory(this.drawingCategories.COMPOSITIONS);
    this.slider.currentSlide(1, 'drawingSlide');
    this._spinner.hide();
  }

  loadCategory(type: string) {
    switch (type) {
      case this.drawingCategories.COMPOSITIONS:
      {
        this.collection = this.compositionCollection;
        this._change.detectChanges();

        break;
      }
      case this.drawingCategories.NATURE:
      {
        this.collection = this.natureCollection;
        this._change.detectChanges();

        break;
      }
      case this.drawingCategories.PORTRAITS:
      {
        this.collection = this.portraitCollection;
        this._change.detectChanges();

        break;
      }
      default: {
        break;
      }
    }
  }

  loadImages() {
    this.compositionCollection = this._imageService.getAllCompositionalDrawingImages();
    this.natureCollection = this._imageService.getAllNatureDrawingImages();
    this.portraitCollection = this._imageService.getAllPortraitDrawingImages();
  }

  showCategory(type: string) {
    this.loadCategory(type);
    this.slider.currentSlide(1, 'drawingSlide');
  }
}
