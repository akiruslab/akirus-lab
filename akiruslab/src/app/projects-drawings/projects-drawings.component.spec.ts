import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsDrawingsComponent } from './projects-drawings.component';

describe('ProjectsDrawingsComponent', () => {
  let component: ProjectsDrawingsComponent;
  let fixture: ComponentFixture<ProjectsDrawingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsDrawingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsDrawingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
