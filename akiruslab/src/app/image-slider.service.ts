import { Inject, Renderer2, Injectable, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class ImageSliderService {
  private  renderer: Renderer2;

  constructor(@Inject(DOCUMENT) document, private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  slideIndex = 1;

  showSlides(slideNum: number, slideType: string) {
    const slides = document.getElementsByClassName(slideType);
    let thumbnails: HTMLCollectionOf<HTMLImageElement>;
    let captionText;

    if (slideNum > slides.length) {
      this.slideIndex = 1;
    }

    if (slideNum < 1) {
      this.slideIndex = slides.length;
    }

    if (slideType === 'photoSlide') {
      thumbnails = <HTMLCollectionOf<HTMLImageElement>> document.getElementsByClassName('demo');
      captionText = document.getElementById('photo-caption');

      for (let i = 0; i < thumbnails.length; i++) {
        this.renderer.removeClass(thumbnails[i], 'active');
      }

      this.renderer.addClass(thumbnails[this.slideIndex - 1], 'active');
      captionText.innerHTML = thumbnails[this.slideIndex - 1].alt;
    }

    for (let i = 0; i < slides.length; i++) {
      this.renderer.removeClass(slides[i], 'show');
      this.renderer.addClass(slides[i], 'hide');
    }

    this.renderer.removeClass(slides[this.slideIndex - 1], 'hide');
    this.renderer.addClass(slides[this.slideIndex - 1], 'show');
  }

  addSlides(slideNum: number, slideType: string) {
    this.showSlides(this.slideIndex += slideNum, slideType);
  }

  currentSlide(slideNum: number,  slideType: string) {
    this.showSlides(this.slideIndex = slideNum, slideType);
  }

  showDrawingSlides(slideNum: number) {
    const view = document.getElementById('scroll-plane');
    let slides: HTMLCollectionOf<HTMLImageElement>;
    slides = <HTMLCollectionOf<HTMLImageElement>> document.getElementsByClassName('partial-view');

    if (slideNum > slides.length) {
      this.slideIndex = 1;
      this.scrollView(view);
    }

    if (slideNum < 1) {
      this.slideIndex = slides.length;
      this.scrollView(view);
    }

    for (let i = 0; i < slides.length; i++) {
      this.renderer.removeClass(slides[i], 'focused');
      this.renderer.addClass(slides[i], 'unfocused');
    }

    this.renderer.removeClass(slides[this.slideIndex - 1], 'unfocused');
    this.renderer.addClass(slides[this.slideIndex - 1], 'focused');
    this.scrollView(view);
  }

  nextSlide(slideNum: number) {
    this.showDrawingSlides(this.slideIndex += slideNum);
  }

  focusSlide(slideNum: number) {
    this.slideIndex = slideNum;
    this.showDrawingSlides(this.slideIndex );
  }

  scrollView(view: HTMLElement) {
    this.renderer.setStyle(view, 'overflow', 'hidden');
    view.scrollTo((240 * this.slideIndex - 1), 0);
  }

}
