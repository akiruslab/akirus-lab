import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BioComponent } from './bio/bio.component';
import { BriefComponent } from './brief/brief.component';
import { ProjectsComponent } from './projects/projects.component';
import { ContactComponent } from './contact/contact.component';
import { ProjectsCodeComponent } from './projects-code/projects-code.component';
import { ProjectsPhotosComponent } from './projects-photos/projects-photos.component';
import { ProjectsVideosComponent } from './projects-videos/projects-videos.component';
import { ProjectsWritingComponent } from './projects-writing/projects-writing.component';
import { HomeComponent } from './home/home.component';
import { ProjectsDrawingsComponent } from './projects-drawings/projects-drawings.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'bio', component: BioComponent},
  {path: 'brief', component: BriefComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'drawings', component: ProjectsDrawingsComponent},
  {path: 'code', component: ProjectsCodeComponent},
  {path: 'photos', component: ProjectsPhotosComponent},
  {path: 'videos', component: ProjectsVideosComponent},
  {path: 'writing', component: ProjectsWritingComponent},
  {path: '**', component: ErrorPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [HomeComponent, BioComponent, BriefComponent,
  ContactComponent, ProjectsComponent, ProjectsDrawingsComponent,
  ProjectsCodeComponent, ProjectsPhotosComponent,
  ProjectsVideosComponent, ProjectsWritingComponent, ErrorPageComponent];
