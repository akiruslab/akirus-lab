import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  private url;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl + 'api/mail/send';
  }

  sendMail(content: any): Observable<Object> {
    console.log('Sending mail now....');

    const response =  this.http.post(this.url, JSON.stringify(content),
      { headers: new HttpHeaders ({ 'Content-type': 'application/json'}),
      responseType: 'text'}).pipe(catchError(
        this.handleError<Object>('sendMail', [])
      ));

    return response;
  }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
