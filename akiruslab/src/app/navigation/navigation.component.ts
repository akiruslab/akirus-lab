import { Component, OnInit } from '@angular/core';
import { faEnvelope, faFlask, faInfoCircle, faAddressCard} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  envelope = faEnvelope;
  flask = faFlask;
  user = faAddressCard;
  info = faInfoCircle;

  constructor() { }

  ngOnInit() {
  }

}
