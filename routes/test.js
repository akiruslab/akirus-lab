const express = require('express');

const router = express.Router();

router.get('/', (req,res,next) =>{
    res.statusCode = 200;
    return res.json({message: 'Hellow World'});
});

module.exports = router;