/**
 * Routes for user related pages:
 * - Login
 * - Profile
 */
//Dependancy imports
const express = require('express');
const passport = require ('passport');
const jwt = require('jsonwebtoken');
const db = require('../config/db');

const User = require('../models/User');

const router = express.Router();
//Login
router.post('/login', (req,res,next)=>{
    const username = req.body.username;
    const password = req.body.password;

    User.getUserByUsername(username, (err,user)=>{
        if(err){
            throw err;
        }
        else if(!user){
            return res.json({success:false, msg: 'No user found'});
            
        }

        User.verifyPassword(password,user.password, (err,isCorrect)=>{
            if(err){
                throw err;
            }
            else if(isCorrect){
                //Creating a signed token that expires in a week
                const token = jwt.sign(user.toJSON(), db.secret, {
                    expiresIn: 604880 
                });

                res.json({
                    success: true,
                    token: 'JWT '+ token,
                    user:{
                        id: user._id,
                        name: user.username,
                        email: user.email
                    }
                });
            }
            else{
                return res.json({success: false, 
                    msg: 'Incorrect details entered'});
            }
        });
    });
});

//Create
router.post('/create', (req,res,next)=>{
    let newUser = new User({
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    });

    User.addUser(newUser, (err,user) =>{
        if(err){
            res.json({
                success: false, 
                msg: "Failed to create user"});
        }
        else{
            res.json({success: true, 
                msg: "User created"});
        }
    });
})

//Profile
router.get('/profile', passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    res.json({user: req.user});
});

module.exports = router;