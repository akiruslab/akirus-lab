const express = require('express');
const mailer = require('nodemailer');

require('dotenv').config();
const router = express.Router();

const transporter = mailer.createTransport({
    host: process.env.MAIL_HOST,
    port: 587,
    secure: false,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
    }
});

router.post('/send', (req,res,next)=> {
    let senderName = req.body.name;
    let senderEmail = req.body.email;
    let senderNumber = req.body.number;
    let subject = req.body.subject;
    let message = req.body.message;
    let sendTo = req.body.sendTo;

    if (senderName === '') {
            res.status(400).send({
            response: 'Bad request - No name provided'
        });
        return;
      }
    
    if (senderEmail === '') {
            res.status(400).send({
            response: 'Bad request - No email provided'
        });
        return;
    }

    if (subject === '') {
            res.status(400).send({
            response: 'Bad request - No subject provided'
        });
        return;
    }

    if (senderNumber === '') {
            res.status(400).send({
            response: 'Bad request - No number provided'
        });
        return;
    }

    if (sendTo === '') {
            res.status(400).send({
            response: 'Bad request - No destination email provided'
        });
        return;
    }

    let body = 'Hi there, \n\n You\'ve received a message from ' + senderName + ':\n\n"' 
    + message + '" \n\n Their number: ' + senderNumber;

    let mail = {
        from: senderName,
        to: sendTo,
        subject: subject,
        text: body,
        replyTo: senderEmail
    }

    transporter.sendMail(mail, (error, response) => {
        if(error) {
            console.log(error);
            res.status(500).send('error')
            return;
        } else {
            console.log(response);
            res.status(200).send('success');
            return;
        }
    });
});

module.exports = router;