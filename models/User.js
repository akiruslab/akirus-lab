/**
 * Users model for user related CRUD
 */
//Dependancy imports
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/db');

//Schema
const UserSchema = mongoose.Schema({
    fullname:{
        type: String
    },
    email:{
        type: String,
        required: true
    },
    username:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id,callback){
    User.findById(id,callback);
}

module.exports.getUserByUsername = function(username,callback){
    const QUERY = {username: username};
    User.findOne(QUERY,callback);
}

module.exports.addUser = function(newUser, callback){
    bcrypt.genSalt(10, (err,salt)=>{
        bcrypt.hash(newUser.password,salt, (err,hash)=>{
            if(err){
                throw err;
            }
            newUser.password = hash;
            newUser.save(callback);
        })
    })
}

module.exports.verifyPassword = function(password, hash, callback){
    bcrypt.compare(password, hash, (err, isCorrect)=>{
        if(err){
            throw err;
        }
        callback(null, isCorrect);    
    });
}