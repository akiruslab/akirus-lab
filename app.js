/**
 * The logic below serves to setup the webserver with Express
 * 
 */
//Dependancy import
const express = require('express');
const path = require('path');
const bodyP = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const users = require('./routes/users');
const mail = require('./routes/mail');
const test = require('./routes/test');
const config = require('./config/db');
require('dotenv').config();

//App Init
const app = express();
app.use(express.static(path.join(__dirname, 'public')));

//DB init
// mongoose.connect(config.database);
// mongoose.connection.on('connected', () =>{
//     console.log('Connected to DB: ' + config.database);
// });
// mongoose.connection.on('error', (err) =>{
//     console.log('Failed to connect to DB: ' + err);
// });

//Middleware init
app.use(cors());
app.use(bodyP.json());

// app.use(passport.initialize());
// app.use(passport.session());

require('./config/passport')(passport);

//Routes
app.use('/api/mail', mail);
app.use('/api/user',users);
app.use('/api/test', test);
app.get('/api', (req,res) =>{
    res.send('Sorry. Nothing here.');
});

//Start Application Server
const port = process.env.PORT || 3000;
app.listen(port, () =>{
    console.log('Server Started: port=' + port);
});
