## Akirus Lab

This is repo contains the initial construction of the Akirus Lab main website. With this, a mission to push the boundaries of the norm has begun and will provide those that are tired of the ordinary a place of ultimate satisfaction through true value from bespoke products.

(Four other pages are set to sprout from this project to represent all aspects of the lab individually.)

[Akirus Lab](http://www.akiruslab.com)

![Alt text](./akiruslab_concept.png?raw=true "akiruslab_concept")